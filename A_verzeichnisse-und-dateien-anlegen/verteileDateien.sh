#!/bin/bash

TEMPLATE_D="/home/luca/m122/A/_template"
SCHULKLASSE_D="/home/luca/m122/A/_schulklasse"
GEN_D="/home/luca/m122/A/_gen"

if [ ! -d "$GEN_D" ]; then
  mkdir -p "$GEN_D"
  echo "Directory $GEN_D created."
else
  echo "Directory $GEN_D already exists."
fi

for class_file in "$SCHULKLASSE_D"/*.txt; do
  class=$(basename "$class_file" .txt)
  
  if [ ! -d "$GEN_D/$class" ]; then
    mkdir -p "$GEN_D/$class"
    echo "Directory $class created."
  else
    echo "Directory $class already exists."
  fi

  while IFS= read -r line; do
    student_dir="$GEN_D/$class/$line"
    
    if [ ! -d "$student_dir" ]; then
      mkdir -p "$student_dir"
      cp "$TEMPLATE_D"/* "$student_dir"
    else
      echo "Directory $line already exists."
    fi
  
  done < "$class_file"
  
  echo "Directories for students created."
done

