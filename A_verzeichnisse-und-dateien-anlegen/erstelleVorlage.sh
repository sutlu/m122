#!/bin/bash

DIRECTORY="/home/luca/m122/"
TEMPLATE_D="/home/luca/m122/A/_template"
SCHULKLASSE_D="/home/luca/m122/A/_schulklasse"

TEMPLATE_FILES=( "datei-1.txt" "datei-2.pdf" "datei-3.doc" )
SCHULKLASSE_FILES=( "M122-PE23a.txt" "M122-PE23b.txt" "M122-PE23c.txt"  )

KLASSE_A=( "Alice" "Bob" "Charlie" "David" "Eva" "Frank" "Grace" "Hannah" "Isaac" "Julia" "Kevin" "Luna" )
KLASSE_B=( "Michael" "Nina" "Oscar" "Paul" "Quincy" "Rachel" "Sam" "Tina" "Uma" "Victor" "Wendy" "Xander" )
KLASSE_C=( "Yara" "Zach" "Anna" "Brian" "Catherine" "Derek" "Elena" "Felix" "George" "Holly" "Ian" "Jack" )

echo "Create Vorlagen und Schulklasse directories:"
echo "-------------------------------------------------"

if [ ! -d "$DIRECTORY" ]; then
  mkdir -p "$DIRECTORY"
  echo "Directory $DIRECTORY created."
else
  echo "Directory $DIRECTORY already exists."
fi

if [ ! -d "$TEMPLATE_D" ]; then
  mkdir -p "$TEMPLATE_D"
  echo "Directory $TEMPLATE_D created."
else
  echo "Directory $TEMPLATE_D already exists."
fi

if [ ! -d "$SCHULKLASSE_D" ]; then
  mkdir -p "$SCHULKLASSE_D"
  echo "Directory $SCHULKLASSE_D created."
else
  echo "Directory $SCHULKLASSE_D already exists."
fi

echo "-------------------------------------------------"
echo "Create files in the directories:"
echo "-------------------------------------------------"

create_files () {
  local directory=$1
  shift
  local files=("$@")

  for file in "${files[@]}"; do
    filepath="$directory/$file"
    if [ ! -e "$filepath" ]; then
      touch "$filepath"
      echo "Created $filepath"
    else
      echo "$file already exists"
    fi
  done
}


create_files "$TEMPLATE_D" "${TEMPLATE_FILES[@]}"
create_files "$SCHULKLASSE_D" "${SCHULKLASSE_FILES[@]}"

echo "-------------------------------------------------"
echo "Fill classes with students"
echo "-------------------------------------------------"

fill_classes () {
  local klasse=$1
  shift
  local students=("$@")
  local filepath="$SCHULKLASSE_D/$klasse"

  for student in "${students[@]}"; do
    filepath="$SCHULKLASSE_D/$klasse"
    echo $student >> $filepath
  done

  echo "Filled $filepath with student names."
}

fill_classes "M122-PE23a.txt" "${KLASSE_A[@]}"
fill_classes "M122-PE23b.txt" "${KLASSE_B[@]}"
fill_classes "M122-PE23c.txt" "${KLASSE_C[@]}"