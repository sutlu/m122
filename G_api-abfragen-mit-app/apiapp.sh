#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

UP='↗'
DOWN='↘'

API_DATA_PATH_CURRENCY='currency/'
API_DATA_PATH_BPI='bpi/'

current_date=$(date +"%Y-%m-%d")

current_timestamp=$(date -d "$current_date" +%s)
previous_timestamp=$((current_timestamp - 86400))
yesterday=$(date -d "@$previous_timestamp" +"%Y-%m-%d")

curl -s https://v6.exchangerate-api.com/v6/bb9ddd1855e84ded5fca04e0/latest/CHF > ${API_DATA_PATH_CURRENCY}chf${current_date}.json
curl -s https://api.coindesk.com/v1/bpi/currentprice.json > ${API_DATA_PATH_BPI}bpi${current_date}.json

current_dollar=$(jq '.conversion_rates.USD' ${API_DATA_PATH_CURRENCY}chf${current_date}.json)
current_euro=$(jq '.conversion_rates.EUR' ${API_DATA_PATH_CURRENCY}chf${current_date}.json)
current_yuan=$(jq '.conversion_rates.CNY' ${API_DATA_PATH_CURRENCY}chf${current_date}.json)
current_rubel=$(jq '.conversion_rates.RUB' ${API_DATA_PATH_CURRENCY}chf${current_date}.json)
current_rupie=$(jq '.conversion_rates.INR' ${API_DATA_PATH_CURRENCY}chf${current_date}.json)

yesterday_dollar=$(jq '.conversion_rates.USD' ${API_DATA_PATH_CURRENCY}chf${yesterday}.json)
yesterday_euro=$(jq '.conversion_rates.EUR' ${API_DATA_PATH_CURRENCY}chf${yesterday}.json)
yesterday_yuan=$(jq '.conversion_rates.CNY' ${API_DATA_PATH_CURRENCY}chf${yesterday}.json)
yesterday_rubel=$(jq '.conversion_rates.RUB' ${API_DATA_PATH_CURRENCY}chf${yesterday}.json)
yesterday_rupie=$(jq '.conversion_rates.INR' ${API_DATA_PATH_CURRENCY}chf${yesterday}.json)

current_bitcoin=$(jq '.bpi.USD.rate_float' ${API_DATA_PATH_BPI}bpi${current_date}.json)
to_chf_bitcoin=$(bc -l <<< "$current_bitcoin / $current_dollar")
chf_bitcoin=$(bc -l <<< "1 / $to_chf_bitcoin")
rounded_bitcoin=$(printf "%.8f" "$chf_bitcoin")

yesterday_bitcoin=$(jq '.bpi.USD.rate_float' ${API_DATA_PATH_BPI}bpi${yesterday}.json)
yesterday_to_chf_bitcoin=$(bc -l <<< "$yesterday_bitcoin / $yesterday_dollar")
yesterday_chf_bitcoin=$(bc -l <<< "1 / $yesterday_to_chf_bitcoin")
yesterday_rounded_bitcoin=$(printf "%.8f" "$yesterday_chf_bitcoin")

compare_and_get_trend() {
    local current_rate=$1
    local yesterday_rate=$2
    local currency=$3
    local trend=

    if (( $(echo "$current_rate > $yesterday_rate" | bc -l) )); then
        trend="$UP"
        colour="$GREEN"
    elif (( $(echo "$current_rate < $yesterday_rate" | bc -l) )); then
        trend="$DOWN"
        colour="$RED"
    else
        trend=""
    fi

    echo "$trend"
    echo "${colour}_"
}

compare_and_get_trend "$current_dollar" "$yesterday_dollar" "dollar"
compare_and_get_trend "$current_euro" "$yesterday_euro" "euro"
compare_and_get_trend "$current_yuan" "$yesterday_yuan" "yuan"
compare_and_get_trend "$current_rubel" "$yesterday_rubel" "rubel"
compare_and_get_trend "$current_rupie" "$yesterday_rupie" "rupie"
compare_and_get_trend "$rounded_bitcoin" "$yesterday_bitcoin" "bitcoin"

# Border
separator() {
  printf "+============+============+============+============+============+============+============+\n"
}

# print data
data() {
  printf "| %-10s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s |\n" "$1" "$2" "$3" "$4" "$5" "$6" "$7"
}


dollar=$(echo -e "${colour}${current_dollar}${NC} ${trend}")
euro=$(echo -e "${colour}${current_euro}${NC} ${trend}")
yuan=$(echo -e "${colour}${current_yuan}${NC} ${trend}")
rubel=$(echo -e "${colour}${current_rubel}${NC} ${trend}")
rupie=$(echo -e "${colour}${current_rupies}${NC} ${trend}")
bitcoin=$(echo -e "${colour}${rounded_bitcoin}${NC} ${trend}")


output="
$(separator)
$(data "1" "$dollar" "$euro" "$yuan" "$rubel" "$rupie" "$bitcoin") 
$(separator)
"
echo "$output" >> market.txt

cat market.txt