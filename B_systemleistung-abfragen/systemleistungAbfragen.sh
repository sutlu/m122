#!/bin/bash

# Border
border() {
  printf "+============================+================================+============================+\n"
}

# separator lines
separator() {
  printf "+----------------------------+--------------------------------+----------------------------+\n"
}

# print data
data() {
  printf "| %-26s | %-30s | %-26s |\n" "$1" "$2" "$3"
}

# displey usage instructions

usage() {
  echo "Usage: $0 [-f]"
  echo "Options:"
  echo "  -f    Write output to a file named with current date and hostname."
  exit 1
}

# Parse command-line options
outputfile=""
while getopts "f" opt; do
  case $opt in
    f)
      # Generate output filename with date and hostname
      current_date=$(date +'%Y%m%d')
      hostname=$(hostname)
      outputfile="${current_date}-sys-${hostname}.log"
      ;;
    \?)
      usage
      ;;
  esac
done

sendmail() {
  local toaddress="$1"
  local subject="$2"
  local body="$3"

  echo "$body" | mail -s "$subject" "$to_address"
}

# Systemlaufzeit
uptime=$(uptime | cut -d "," -f 1 | cut -d " " -f 4,5)

uptime_readable=$(uptime -p)

uptime_weeks=$(echo "$uptime_readable" | grep -oP '\d+(?= week)' || echo 0)
uptime_days=$(echo "$uptime_readable" | grep -oP '\d+(?= day)' || echo 0)

total_days=$((uptime_weeks * 7 + uptime_days))

if [ "$total_days" -lt 8 ]; then
  uptime_status="OK"
else
  uptime_status="NOT-OK"
fi

# Die Grösse des belegten und freien Speicher
storage_total=$(df --total --block-size=1G | grep 'total' | awk '{print $2}')
storage_free=$(df --total --block-size=1G | grep 'total' | awk '{print $4}')
storage_usage=$(df --total --block-size=1G | grep 'total' | awk '{print $5}' | sed 's/%//')

if [ "$storage_usage" -lt 75 ]; then
  storage_status="OK"
else
  storage_status="NOT OK"
fi

#  Hostname und IP
hostname=$(hostname)
ip=$(hostname -I | cut -d " " -f 1)

# Betriebsystemname und Version
sysname=$( grep '^NAME=' /etc/os-release | cut -d '"' -f 2 )
version=$( grep '^VERSION=' /etc/os-release | cut -d '"' -f 2 | cut -d ' ' -f 1)

# Der Modellname der CPU und die Anzahl der CPU-Cores
cpumodel=$( grep 'model name' /proc/cpuinfo | uniq | awk -F ': ' '{print $2}' )
numbofcores=$( grep -m 1 'cpu cores' /proc/cpuinfo | awk -F ': ' '{print $2}' )

# gesamter und benutzter arbeitsspeicher
totalmemory=$( free -h | awk '/^Mem:/ {print $2}' )
usedmemory=$( free -h | awk '/^Mem:/ {print $3}' )

outputcontent="
$(border)
$(data "Uptime:" "$uptime" "$uptime_status")
$(separator)
$(data "Storage Total:" "${storage_total}G" "$storage_status")
$(data "Storage available:" "${storage_free}G" " ")
$(separator)
$(data "hostname & IP:" "$hostname" "$ip")
$(separator)
$(data "Operating System:" "$sysname" "$version")
$(separator)
$(data "CPU and No of Cores" "$cpumodel" "$numbofcores")
$(separator)
$(data "Free and used RAM" "$totalmemory" "$usedmemory")
$(border)
"

#html for webserver
html_content="
<!DOCTYPE html>
<html>
<head>
  <title>System Data</title>
  <style>
    table {
      width: 100%;
      border-collapse: collapse;
    }
    th, td {
      padding: 10px;
      text-align: left;
      border-bottom: 1px solid #ddd;
    }
    th {
      background-color: #f2f2f2;
    }
    .status-ok {
      background-color: green;
      color: white;
    }
    .status-not-ok {
      background-color: red;
      color: white;
    }
  </style>
</head>
<body>
  <h2>System Data Report</h2>
  <table>
    <tr>
      <th>Category</th>
      <th>Value</th>
      <th>Status</th>
    </tr>
    <tr>
      <td>Uptime</td>
      <td>$uptime</td>
      <td class='status-${uptime_status,,}'>$uptime_status</td>
    </tr>
    <tr>
      <td>Storage Total</td>
      <td>${storage_total}G</td>
      <td class='status-${storage_status,,}'>$storage_status</td>
    </tr>
    <tr>
      <td>Storage available</td>
      <td>${storage_free}G</td>
      <td></td>
    </tr>
    <tr>
      <td>Hostname & IP</td>
      <td>$hostname</td>
      <td>$ip</td>
    </tr>
    <tr>
      <td>Operating System</td>
      <td>$sysname</td>
      <td>$version</td>
    </tr>
    <tr>
      <td>CPU and No of Cores</td>
      <td>$cpumodel</td>
      <td>$numbofcores</td>
    </tr>
    <tr>
      <td>Free and used RAM</td>
      <td>$totalmemory</td>
      <td>$usedmemory</td>
    </tr>
  </table>
</body>
</html>
"



currtime=$(date +'%H:%M:%S')
currdate=$(date +'%Y-%m-%d')
pathtooutput="/var/log/sysdata/"

toaddress="lucasuter@icloud.com"
subject="m122 System data Report"
body=$(outputcontent)

if [ -n "$outputfile" ]; then
  # File to logs
  echo "System data from ${currdate}, ${currtime}:" >> "${pathtooutput}${outputfile}"
  echo "$outputcontent" >> "${pathtooutput}${outputfile}"
  echo "Output written to ${pathtooutput}${outputfile}"
  # to html
  echo "$html_content" > index.html
  echo "HTML report written to index.html"
  # send mail to me
  sendmail "$toaddress" "$subject" "$body"
else
  echo "$outputcontent"
fi

ftpserver="ftp.haraldmueller.ch"
remotedir="/suter"
ftpuser="schueler"
ftppassword="studentenpasswort"
filepath="index.html"

ftp -inv $ftpserver <<EOF
user $ftpuser $ftppassword
mkdir $remotedir
cd $remotefile
put $filepath
bye
EOF