#!/bin/bash

usage() {
  echo "Usage: $0 -f <folder> [-s <user@server:/path>]"
  exit 1
}

while getopts ":f:s:" opt; do
  case ${opt} in
    f )
      folder=$OPTARG
      ;;
    s )
      server=$OPTARG
      ;;
    \? )
      usage
      ;;
    : )
      echo "Option -$OPTARG requires an argument."
      usage
      ;;
  esac
done

current_date=$(date +%Y-%m-%d_%H%M%S)
hostname=$(hostname)
dir_name=$(basename $folder)
backup_file="/backup/${dir_name}_${current_date}.tar.gz"
log_dir="/var/log/backup"
log_file="backup.log"


mkdir -p $log_dir
mkdir -p /backup

if [ -z "$folder" ]; then
  echo "Folder to backup is required."
  usage
fi

echo "Creating backup of $folder..."
tar -czf $backup_file $folder

if [ $? -ne 0 ]; then
  echo "Failed to create backup of $folder."
  echo "[ ${current_date} ]: Backup failed." >> $log_dir/$log_file
  exit 1
fi

echo "Backup created: $backup_file"
echo "[ ${current_date} ]: Backup created of ${folder}" >> $log_dir/$log_file

if [ ! -z "$server" ]; then
  echo "Uploading $backup_file to $server..."
  scp $backup_file $server

  if [ $? -ne 0 ]; then
    echo "Failed to upload $backup_file to $server."
    echo "[ ${current_date} ]: Failed to upload $backup_file to $server." >> $log_dir/$log_file
    exit 1
  fi

  echo "Backup uploaded to $server"
  echo "[ ${current_date} ]: Backup uploaded to $server" >> $log_dir/$log_file
fi

echo "Backup process completed."