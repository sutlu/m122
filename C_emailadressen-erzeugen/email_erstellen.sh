#!/bin/bash

curl https://haraldmueller.ch/schueler/m122_projektunterlagen/b/MOCK_DATA.csv > mock_data.csv

people_file="mock_data.csv"
current_date=$(date +"%Y-%m-%d_%H-%m-%S")
outputfile="${current_date}_mailimport.csv"
readable_date=$(date +"%d.%m.%Y")
archive_name="$(date +"%Y-%m-%d")_newMailadr_PE23d_Suter.tar"
logfile_dir="/var/log/mailcreator"

if [ ! -d "$logfile_dir" ]; then
  sudo mkdir -p "$logfile_dir"
else
  echo "Verzeichnis $logfile_dir existiert bereits." >> /dev/null
fi

sanitize_name() {
  local name="$1"
  name_sanitized=$(echo "$name" | tr '[:upper:]' '[:lower:]' | sed -e 's/ä/ae/g' \
  -e 's/ä/ae/g' \
  -e 's/Ä/ae/g' \
  -e 's/ü/ue/g' \
  -e 's/Ü/ue/g' \
  -e 's/ö/oe/g' \
  -e 's/Ö/oe/g' \
  -e 'y/áÁàÀâÂåÅéÉèÈêÊëËìÌíÍîÎïÏóÓòÒôÔúÚùÙûÛçÇ/aaaaaaaaeeeeeeeeiiiiiiiioooooouuuuuucc/' \
  -e 's/ /_/g' \
  -e "s/'/_/g")
  echo "$name_sanitized"
}


doubletten_kontrolle() {
  sort email_namen.txt | uniq -c | while read -r count name; do
    if [ "$count" -gt 1 ]; then
      for i in $(seq 1 $count); do
        echo "$name" >> duplicates.txt
      done
    else
      echo "$name" >> temp.txt
    fi
  done
  mv temp.txt email_namen.txt
}

create_unique_email_for_doubles() {
  while IFS=',' read -r name rest; do
    random_number=$(shuf -i 1000-9999 -n 1)
    echo "${name}${random_number}" >> email_namen.txt
  done < duplicates.txt
}

create_mail_accounts() {
  while IFS=',' read -r name rest; do
    password=$(tr -dc 'A-Za-z0-9' < /dev/urandom | head -c 12)
    echo "${name}@edu.tbz.ch;${password}" >> $outputfile
  done < email_namen.txt
  cp $outputfile tempoutfile.csv
}



tail -n +2 "$people_file" | while IFS=',' read -r id vorname nachname rest; do
  sanitized_vorname=$(sanitize_name "$vorname")
  sanitized_nachname=$(sanitize_name "$nachname")
  email_name="${sanitized_vorname}.${sanitized_nachname}"
  echo $email_name >> email_namen.txt
done


create_letter() {

  mkdir -p letters

  tail -n +2 "$people_file" | while IFS=',' read -r id vorname nachname geschlecht strasse hausnummer plz ort; do 
      
      if [[ "$geschlecht" == "Male" ]]; then
        anrede="Liebe ${vorname}"
      else
        anrede="Liebe ${vorname}"
      fi

      mailaddr=$(grep "$(sanitize_name ${vorname}).$(sanitize_name ${nachname})" tempoutfile.csv | cut -d ';' -f 1 | tail -n 1)
      pw=$(grep "$(sanitize_name ${vorname}).$(sanitize_name ${nachname})" tempoutfile.csv | cut -d ';' -f 2 | tail -n 1)
      sed -i "/${mailaddr}/d" tempoutfile.csv


      letter_content="Technische Berufsschule Zürich
Ausstellungsstrasse 70
8005 Zürich

Zürich, den $readable_date

                        $vorname $nachname
                        $strasse $hausnummer
                        $plz $ort
                        

$anrede

Es freut uns, Sie im neuen Schuljahr begrüssen zu dürfen.

Damit Sie am ersten Tag sich in unsere Systeme einloggen
können, erhalten Sie hier Ihre neue Emailadresse und Ihr
Initialpasswort, das Sie beim ersten Login wechseln müssen.

Emailadresse:   $mailaddr
Passwort:       $pw


Mit freundlichen Grüssen

Luca Suter
(TBZ-IT-Service)


admin.it@tbz.ch, Abt. IT: +41 44 446 96 60"



      echo "$letter_content" > "letters/${mailaddr}.brf"
      echo "[ ${current_date} ] The letter for ${mailaddr} has been created." >> "${logfile_dir}/mailcreator.log"
  done
}



doubletten_kontrolle
create_unique_email_for_doubles
create_mail_accounts
create_letter

tar -cvf "$archive_name" "$outputfile" letters/*.brf

rm tempoutfile.csv email_namen.txt duplicates.txt mock_data.csv

ftpserver="ftp.haraldmueller.ch"
remotedir="/suter/mail"
ftpuser="schueler"
ftppassword="studentenpasswort"
filepath="${archive_name}"

ftp -inv $ftpserver <<EOF
user $ftpuser $ftppassword
mkdir $remotedir
cd $remotedir
put $filepath
bye
EOF

echo "=============================================================================="
echo "The Mails and letters for all Students in the current import have been Created"
echo "------------------------------------------------------------------------------"
echo "${output_file} has been created."
echo "------------------------------------------------------------------------------"
echo "The letters for the students and the ${outputfile} have been archived"
echo "The archive file has been uoloaded to ${fptserver}"
echo "=============================================================================="